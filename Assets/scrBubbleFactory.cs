﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;

public class scrBubbleFactory : MonoBehaviour
{
    public List<GameObject> bubbles;
    public GameObject bubbleModel;
    public InputField mainInputField;

    public void Start()
    {
        //Adds a listener to the main input field and invokes a method when the value changes.
        mainInputField.onEndEdit.AddListener(delegate { ValueChangeCheck(); });
    }

    // Invoked when the value of the text field changes.
    public void ValueChangeCheck()
    {
        Debug.Log("Value Changed");
        byte[] bytes;
        bytes = Encoding.UTF8.GetBytes(mainInputField.text);
        StartCoroutine(Upload(bytes));
        mainInputField.text = "";
    }

    // Update is called once per frame
    public void NewMessage(string message)
    {
        var temp = Instantiate(bubbleModel);
        temp.transform.SetParent(gameObject.transform);
        temp.transform.localPosition = new Vector3(0, 0, 0);
        temp.transform.Find("Text").GetComponent<Text>().text = message;
        bubbles.Add(temp);
        foreach (GameObject bubble in bubbles)
        {
            var y = bubble.transform.position.y;
            bubble.transform.localPosition = new Vector3 (0, y + 30 , 0) ;
        }
    }
    IEnumerator Upload(byte[] data)
    {
        WWWForm form = new WWWForm();

        using (UnityWebRequest www = UnityWebRequest.Post("https://unitycontentmod.cognitiveservices.azure.com/contentmoderator/moderate/v1.0/ProcessText/Screen?classify=True", form)) // URL aqui
        {
            www.SetRequestHeader("Ocp-Apim-Subscription-Key", ""); //KEY aqui
            www.SetRequestHeader("Content-Type", "text/plain");
            www.uploadHandler = new UploadHandlerRaw(data);
            www.uploadHandler.contentType = "text/plain";
            www.downloadHandler = new DownloadHandlerBuffer();

            yield return www.SendWebRequest();
            Debug.Log(data);
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string jsonResponse = www.downloadHandler.text;
                
                
                
                AnalysisObject analysisObject = new AnalysisObject();
                analysisObject = JsonConvert.DeserializeObject<AnalysisObject>(jsonResponse);
                string ResultText = analysisObject.OriginalText;
                if(analysisObject.Terms != null)
                {
                    foreach (Term predict in analysisObject.Terms)
                    {
                        
                        var n = predict.term.Length;
                        string censor = "";
                        for (int i = 0; i < n; i++)
                        {
                            censor = censor + '*';

                        }
                        ResultText = ResultText.Replace(predict.term, censor);
                    }
                }
                

                NewMessage(ResultText);
            }
        }
    }

}
[Serializable]
public class AnalysisObject
{
    public List<Term> Terms { get; set; }
    public string OriginalText { get; set; }
}

[Serializable]
public class Term
{
    public int Index { get; set; }
    public int OriginalIndex { get; set; }
    public int ListId { get; set; }
    public string term { get; set; }
}



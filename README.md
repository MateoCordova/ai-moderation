# Unity with Azure: Content Moderator
This repo contains a simple example of the use of the Text Content Moderation API as user send messages to a "chat".

## Important links:


Content Moderation [Link](https://docs.microsoft.com/es-es/azure/cognitive-services/content-moderator/api-reference)


Text API Documentacion [Link](https://westus.dev.cognitive.microsoft.com/docs/services/57cf753a3f9b070c105bd2c1/operations/57cf753a3f9b070868a1f66f)
